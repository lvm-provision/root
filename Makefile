
SHELL:=bash

lvm_provision_dir_rel=lvm-provision
lvm_provision_bash_sources_dir_rel=$(lvm_provision_dir_rel)/sh
lvm_provision_doc_dir_rel=$(lvm_provision_dir_rel)/doc
lvm_provision_asciio_dir_rel=$(lvm_provision_doc_dir_rel)/asciio
lvm_tester_dir_rel=lvm-tester
lvm_tester_config_rel=$(lvm_tester_dir_rel)/config.sh
target_dir_rel=target
naturaldocs_project_dir_rel=naturaldocs
naturaldocs_output_dir_rel=$(target_dir_rel)/naturaldocs
lvm_provision_asciio_output_dir_rel=$(target_dir_rel)/asciio

ASCIIO_SRC=$(wildcard $(lvm_provision_asciio_dir_rel)/*.asciio)
ASCIIO_OBJ=$(notdir $(patsubst %.asciio,%.txt,$(ASCIIO_SRC)))
ASCIIO_TARGET=$(addprefix $(lvm_provision_asciio_output_dir_rel), $(ASCIIO_OBJ))

$(info $$ASCIIO_SRC is [${ASCIIO_SRC}])
$(info $$ASCIIO_OJB is [${ASCIIO_OBJ}])
$(info $$ASCIIO_TARGET is [${ASCIIO_TARGET}])

naturaldocs: $(lvm_provision_bash_sources_dir_rel)/*.sh $(lvm_tester_config_rel)
	naturaldocs \
		--project $(naturaldocs_project_dir_rel) \
		--input $(lvm_provision_bash_sources_dir_rel) \
		--input $(lvm_tester_bash_sources_dir_rel) \
		--output HTML $(naturaldocs_output_dir_rel)

asciio: $(ASCIIO_TARGET)

$(ASCIIO_TARGET): $(ASCIIO_SRC)
	asciio_to_text $<  > $@

clean:
	rm -Rf $(naturaldocs_output_dir_rel)
	mkdir -p $(naturaldocs_output_dir_rel)

shellcheck: $(lvm_provision_bash_sources_dir_rel)/*.sh $(lvm_tester_config_rel)
	shellcheck `find . -type f -iname '*.sh'`

run: shellcheck
	bash -x $(lvm_provision_dir_rel)/sh/remote/lvm-provision.sh --project-dir=$(lvm_tester_dir_rel)

git-commit:
	# lvm-provision
	pushd "$(lvm_provision_dir_rel)" ; \
	git add . ; \
	git commit -m "$(message)" ; \
	popd
	# lvm-tester
	pushd "$(lvm_tester_dir_rel)" ; \
	git add . ; \
	git commit -m "$(message)" ; \
	popd
	# root
	git add . ; \
	git commit -m "$(message)"

git-push:
	# lvm-provision
	pushd "$(lvm_provision_dir_rel)" ; \
	git push ; \
	popd
	# lvm-tester
	pushd "$(lvm_tester_dir_rel)" ; \
	git push ; \
	popd
	# root
	git push

git-diff:
	less -f -r <(cd lvm-provision ; git diff --color=always) <(cd lvm-tester ; git diff --color=always) <(git diff --color=always)

